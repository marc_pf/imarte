<?php

namespace ImarteW;

class Separator extends Widget {

	function __construct() {
		parent::__construct( 'Imarte Separator' );
	}

	// Creating widget front-end
	public function get_html_content( $instance, $args ) {
		return "<hr class='imarte-separator'/>";
	}

	public function get_widget_form() {
		return array(
			'customize' => array(
				'type' => 'section',
				'label' => _x('Customize', 'Widget Section Label', DOMAIN_NAMESPACE),
				'hide' => true,
				'fields' => array(
					'color' => array(
						'type' => 'color',
						'label' => __('Color', DOMAIN_NAMESPACE),
					),
					'height' => array(
						'type' => 'measurement',
						'label' => __('Height', DOMAIN_NAMESPACE),
					),
				),
			),
		);
	}

	static function less_vars_def($instance) {
		return array(
			'color' => array('customize', 'color'),
			'height' => array('customize', 'height'),
		);
	}
}
