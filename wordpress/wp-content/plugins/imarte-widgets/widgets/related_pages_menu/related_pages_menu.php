<?php

namespace ImarteW;

class Related_Pages_Menu extends Widget {

	function __construct() {
		parent::__construct( 'Imarte Related Pages Menu' );
	}

	function init_styles() {
		$css_file = widget_dir('menu_common') . 'css/style.css';
		$css_url = widget_url('menu_common') . 'css/style.css';

		if ( file_exists($css_file) ) {
			$this->register_frontend_styles(
				array(
					array(
						'imartew_menu',
						$css_url,
						array()
					),
				)
			);
		}
	}

	// Creating widget front-end
	public function get_html_content( $instance, $args ) {
		$pages = $this->get_related_pages($this->get_page_id( $instance ), $instance['relation']);
		$output = '';
		$item_class = 'imarte-page-menu__item';
		$is_button = $instance['type'] === 'buttons';
		if ($is_button) $item_class .= ' imarte-page-menu__item--btn imarte-btn';
		foreach ($pages as $page) {
			$is_current = $page->ID === get_queried_object_ID();
			$permalink = get_the_permalink($page->ID);
			$class = $is_current ? $item_class . ' current' : $item_class;
			$item = "<a class='{$class}' href='{$permalink}'>{$page->post_title}</a>";

			if (!$is_button) {
				$li_class = "imarte-page-menu__li imarte-page-menu__li--{$instance['columns']}";
				if ($is_current) $li_class .= ' current';
				$item = "<li class='{$li_class}'>{$item}</li>";
			}
			$output .= $item;
		}
		$tag = $is_button ? 'nav' : 'ul';
		$output && $output = "<{$tag} class='imarte-page-menu imarte-page-menu--related imarte-page-menu--{$instance['type']}'>{$output}</{$tag}>";
		return $output;
	}

	public function get_widget_form() {
		return array(

			'relation' => array(
				'type' => 'select',
				'label' => _x('Pages', 'Widget Section Label', DOMAIN_NAMESPACE),
				'options' => array(
					'child' => __('Child Pages', DOMAIN_NAMESPACE),
					'sibling' => __('Sibling Pages', DOMAIN_NAMESPACE),
				),
			),
			'type' => array(
				'type' => 'select',
				'label' => _x('Type', 'Widget Field Label', DOMAIN_NAMESPACE),
				'options' => array(
					'buttons' => __('Buttons', DOMAIN_NAMESPACE),
					'list' => __('List', DOMAIN_NAMESPACE),
				),
				'default' => 'buttons',
				'state_emitter' => array(
					'callback' => 'select',
					'args' => array( 'nav_type' )
				),
			),
			'columns' => array(
				'type' => 'select',
				'label' => _x('Columns', 'Widget Field Label', DOMAIN_NAMESPACE),
				'options' => array(
					1 => 1,
					2 => 2,
					3 => 3,
					4 => 4,
					5 => 5,
					6 => 6,
					7 => 7,
					8 => 8,
				),
				'state_handler' => array(
					'nav_type[list]' => array( 'show' ),
					'_else[nav_type]' => array( 'hide' ),
				),
			),
			'reference' => array(
				'type' => 'section',
				'label' => _x('Reference Page', 'Widget Section Label', DOMAIN_NAMESPACE),
				'hide' => true,
				'fields' => array(
					'post' => array(
						'type' => 'link',
						'post_types' => array('page'),
						'label' => _x('(Empty = current page)', 'Widget Section Label', DOMAIN_NAMESPACE),
					),
				),
			),
		);
	}

	private function get_page_id($instance) {
		$reference_post = $instance['reference']['post'];
		if ( $reference_post === '' ) {
			return get_the_ID();
		}
		elseif ( strpos($reference_post, 'post: ') === 0 ) {
			$page_id = substr($reference_post, 6);
			return $page_id;
		}
		return 0;
	}

	private function get_related_pages($post_id, $relation = 'child') {
		$post = get_post($post_id);
		if (!$post) return array();
		switch ($relation) {
			case 'child':
				return get_children(array(
					'post_parent' => $post->ID,
					'post_type'   => $post->post_type,
					'numberposts' => -1,
					'post_status' => 'publish',
					'orderby'     => 'menu_order',
					'order'       => 'ASC',
				));
			case 'sibling':
				if (! $post->post_parent) return array();

				return get_children(array(
					'post_parent' => $post->post_parent,
					'post_type'   => $post->post_type,
					'numberposts' => -1,
					'post_status' => 'publish',
					'orderby'     => 'menu_order',
					'order'       => 'ASC',
				));

		}
		return array();
	}
}
