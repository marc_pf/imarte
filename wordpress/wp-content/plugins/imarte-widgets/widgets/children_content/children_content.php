<?php

namespace ImarteW;

$imartew_children_content_rendering = array();

class Children_Content extends Widget {

	static $rendering_loop = false;

	function __construct() {
		parent::__construct( 'Imarte Child Pages Content' );
	}

	// Creating widget front-end
	public function get_html_content( $instance, $args ) {
		if( empty( $instance['template'] ) ) return;
		if( is_admin() ) return;

		$rendering =& $GLOBALS['imartew_children_content_rendering'];

		$title_style =& $GLOBALS['imartew_children_content_title_style'];
		$prev_title_style = $title_style;

		if ( isset($instance['title_style']) && $instance['title_style'] !== 'inherit' )
			$title_style = $instance['title_style'];

		$title_tag =& $GLOBALS['imartew_children_content_title_tag'];
		$prev_title_tag = $title_tag;

		if (  empty( $instance['title_tag'] ) ) {
			if ( empty( $title_tag ) ) {
				$title_tag = 2;
			}
			elseif ( $title_tag < 6 ) $title_tag++;
		}
		else {
			$title_tag = $instance['title_tag'];
		}

		$output = '';

		static $depth = 0;
		$depth++;
		// if ($depth > 1) {
		// 	$depth--;
		// 	return $output;
		// }

		$form_id = $instance['_sow_form_id'];

		if ( !empty($rendering[ $form_id ]) ) return $output;
		$rendering[ $form_id ] = true;

		// if ( static::$rendering_loop ) return $output;
		static::$rendering_loop = true;

		$reference_post = get_post( $this->get_page_id( $instance ) );

		global $siteorigin_panels_current_post;

		$children_query = new \WP_Query(array(
			'post_parent' => $reference_post->ID,
			'post_type'   => $reference_post->post_type,
			'numberposts' => -1,
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'orderby'     => 'menu_order',
			'order'       => 'ASC',
			'paged'       => 1,
			'post__not_in' => array($siteorigin_panels_current_post),
		));

		ob_start();

		if(strpos('/'.$instance['template'], '/content') !== false) {
			while( $children_query->have_posts() ) {
				$children_query->the_post();
				locate_template($instance['template'], true, false);
			}
		}
		else {
			$old_query = $GLOBALS['wp_query'];
			$GLOBALS['wp_query'] = $children_query;
			locate_template($instance['template'], true, false);
			$GLOBALS['wp_query'] = $old_query;
		}

		$output .= ob_get_clean();

		// Reset everything
		// wp_reset_query();
		wp_reset_postdata();

		unset( $rendering[ $form_id ] );
		$depth--;
		static::$rendering_loop = false;
		$title_style = $prev_title_style;
		$title_tag = $prev_title_tag;

		return $output;
	}

	public function get_widget_form() {
		return array(
			'template' => array(
				'type' => 'select',
				'label' => __( 'Template', 'siteorigin-panels' ),
				'options' => $this->get_template_options(),
				'default' => 'loop.php',
			),
			'title_tag' => array(
				'type' => 'select',
				'label' => _x('Page Titles Tag', 'Widget Field Label', DOMAIN_NAMESPACE),
				'options' => array(
					0 => 'Descend',
					1 => 'h1',
					2 => 'h2',
					3 => 'h3',
					4 => 'h4',
					5 => 'h5',
					6 => 'h6',
				),
				'default' => 0,
			),
			'title_style' => array(
				'type' => 'select',
				'label' => _x('Page Titles Style', 'Widget Field Label', DOMAIN_NAMESPACE),
				'options' => array(
					'default' => __('Default', DOMAIN_NAMESPACE),
					'plain' => __('Plain', DOMAIN_NAMESPACE),
					'section' => __('Section Title', DOMAIN_NAMESPACE),
					'hide' => __('Hide', DOMAIN_NAMESPACE),
					'inherit' => __('Inherit', DOMAIN_NAMESPACE),
				),
				'default' => 'default',
			),
			'reference' => array(
				'type' => 'section',
				'label' => _x('Reference Page', 'Widget Section Label', DOMAIN_NAMESPACE),
				'hide' => true,
				'fields' => array(
					'post' => array(
						'type' => 'link',
						'post_types' => array('page'),
						'label' => _x('(Empty = current page)', 'Widget Section Label', DOMAIN_NAMESPACE),
					),
				),
			),
		);
	}

	private function get_page_id($instance) {
		$reference_post = $instance['reference']['post'];
		if ( $reference_post === '' ) {
			return get_the_ID();
		}
		elseif ( strpos($reference_post, 'post: ') === 0 ) {
			$page_id = substr($reference_post, 6);
			return $page_id;
		}
		return 0;
	}

	function get_template_options(){
		$templates = array();

		$template_files = array(
			'loop*.php',
			'*/loop*.php',
			'content*.php',
			'*/content*.php',
		);

		$template_dirs = array( get_template_directory(), get_stylesheet_directory() );
		$template_dirs = array_unique( $template_dirs );
		foreach( $template_dirs  as $dir ){
			foreach( $template_files as $template_file ) {
				foreach( (array) glob($dir.'/'.$template_file) as $file ) {
					if( file_exists( $file ) ) $templates[] = str_replace($dir.'/', '', $file);
				}
			}
		}

		$templates = array_unique( $templates );
		$templates = apply_filters('siteorigin_panels_postloop_templates', $templates);
		sort( $templates );

		$template_options = array();
		if( ! empty( $templates ) ) {
			foreach( $templates as $template ) {
				$headers = get_file_data( locate_template( $template ), array(
					'loop_name' => 'Loop Name',
				) );
				$template_options[ $template ] = esc_html( ! empty( $headers['loop_name'] ) ? $headers['loop_name'] : $template );
			}
		}

		return $template_options;
	}
}
