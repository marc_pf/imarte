<?php

namespace ImarteW;

class Single_Page_Menu extends Widget {

	function __construct() {
		parent::__construct( 'Imarte Single Page Menu' );
	}

	function init_styles() {
		$css_file = widget_dir('menu_common') . 'css/style.css';
		$css_url = widget_url('menu_common') . 'css/style.css';

		if ( file_exists($css_file) ) {
			$this->register_frontend_styles(
				array(
					array(
						'imartew_menu',
						$css_url,
						array()
					),
				)
			);
		}
	}

	// Creating widget front-end
	public function get_html_content( $instance, $args ) {
		$output = '';
		$item_class = 'imarte-page-menu__item';
		$is_button = $instance['type'] === 'buttons';
		if ($is_button) $item_class .= ' imarte-page-menu__item--btn imarte-btn';
		foreach ($instance['buttons'] as $button) {
			$item = "<a class='$item_class' href='#{$button['id']}'>{$button['title']}</a>";
			if (!$is_button) $item = "<li class='imarte-page-menu__li imarte-page-menu__li--{$instance['columns']}'>{$item}</li>";
			$output .= $item;
		}
		$tag = $is_button ? 'nav' : 'ul';
		$output && $output = "<{$tag} class='imarte-page-menu imarte-page-menu--single imarte-page-menu--{$instance['type']}'>{$output}</{$tag}>";
		return $output;
	}

	public function get_widget_form() {
		return array(
			'buttons' => array(
				'type' => 'repeater',
				'label' => _x('Items', 'Widget Section Label', DOMAIN_NAMESPACE),
				'item_name' => _x('Item', 'Widget Repeater Item Label', DOMAIN_NAMESPACE),
				'fields' => array(
					'title' => array(
						'type' => 'text',
						'label' => _x('Title', 'Widget Field Label', DOMAIN_NAMESPACE),
					),
					'id' => array(
						'type' => 'text',
						'label' => _x('ID', 'Widget Field Label', DOMAIN_NAMESPACE),
					),
				),
			),
			'type' => array(
				'type' => 'select',
				'label' => _x('Type', 'Widget Field Label', DOMAIN_NAMESPACE),
				'options' => array(
					'buttons' => __('Buttons', DOMAIN_NAMESPACE),
					'list' => __('List', DOMAIN_NAMESPACE),
				),
				'default' => 'buttons',
				'state_emitter' => array(
					'callback' => 'select',
					'args' => array( 'nav_type' )
				),
			),
			'columns' => array(
				'type' => 'select',
				'label' => _x('Columns', 'Widget Field Label', DOMAIN_NAMESPACE),
				'options' => array(
					1 => 1,
					2 => 2,
					3 => 3,
					4 => 4,
					5 => 5,
					6 => 6,
					7 => 7,
					8 => 8,
				),
				'state_handler' => array(
					'nav_type[list]' => array( 'show' ),
					'_else[nav_type]' => array( 'hide' ),
				),
			),
		);
	}
}
