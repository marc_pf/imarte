<?php

namespace ImarteW;

class Image extends Widget {

	function __construct() {
		parent::__construct( 'Imarte Image' );
	}

	// Creating widget front-end
	public function get_html_content( $instance, $args ) {
		$image = empty( $instance['media'] ) ? get_the_post_thumbnail(null, $instance['size'])
			: wp_get_attachment_image($instance['media'], $instance['size']);


		if ( !empty($instance['image_link']) ) {
			$link = empty( $instance['media'] ) ? get_the_post_thumbnail_url(null, 'full')
				: wp_get_attachment_image_src($instance['media'], 'full');

			$link_class = 'js-lightbox';
		}
		elseif ( !empty($instance['link']) ) {
			$link = sow_esc_url( $instance['link'] );
			$link_class = '';
		}

		if ( !empty($instance['new_window']) ) {
			$target = '_blank';
		}
		else {
			$target = '';
		}

		if ( !empty($link) ) $image = "<a href='{$link}' class='{$link_class}' target='{$target}' >{$image}</a>";
		return "<figure class='imarte-image'>{$image}</figure>";
	}

	public function get_widget_form() {

		return array(
			'media' => array(
				'type' => 'media',
				'label' => _x('Chose image. Leave empty to use Featured Image', 'Widget Section Label', DOMAIN_NAMESPACE),
				'library' => 'image',
				'fallback' => false,
			),
			'size' => array(
				'type' => 'image-size',
				'label' => __('Size', DOMAIN_NAMESPACE),
			),
			'align' => array(
				'type' => 'select',
				'label' => __('Alignment', DOMAIN_NAMESPACE),
				'default' => 'default',
				'options' => array(
					'default' => __('Default', DOMAIN_NAMESPACE),
					'left' => __('Left', DOMAIN_NAMESPACE),
					'right' => __('Right', DOMAIN_NAMESPACE),
					'center' => __('Center', DOMAIN_NAMESPACE),
				),
			),
			'image_link' => array(
				'type' => 'checkbox',
				'default' => false,
				'label' => __('Link to Image', DOMAIN_NAMESPACE),
			),
			'link' => array(
				'type' => 'link',
				'label' => __('Link', DOMAIN_NAMESPACE),
			),
			'new_window' => array(
				'type' => 'checkbox',
				'default' => false,
				'label' => __('Open in new window', DOMAIN_NAMESPACE),
			),
		);
	}

	// static function less_vars_def($instance) {
	// 	return array(
	// 		'color' => array('customize', 'color'),
	// 	);
	// }
}
