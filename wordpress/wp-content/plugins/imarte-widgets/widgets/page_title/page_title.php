<?php

namespace ImarteW;

class Page_Title extends Widget {

	function __construct() {
		parent::__construct( 'Imarte Page Title' );
	}

	// Creating widget front-end
	public function get_html_content( $instance, $args ) {
		$title = esc_html( get_the_title() );
		$tag = isset( $GLOBALS['imartew_children_content_title_tag'] ) ? $GLOBALS['imartew_children_content_title_tag'] : 1;
		$class;
		$title_style =& $GLOBALS['imartew_children_content_title_style'];
		switch (isset($title_style) ? $title_style : '') {
			case 'hide':
				return '';

			case 'plain':
				$class = 'imarte-plain-title';
				break;
			case 'section':
				$class = 'imarte-section-title';
				break;
			default:
				$class = 'page-title';
		}
		return "<h{$tag} class='{$class}'>{$title}</h1>";
	}

	public function get_widget_form() {

		return array(
			'customize' => array(
				'type' => 'section',
				'label' => _x('Customize', 'Widget Section Label', DOMAIN_NAMESPACE),
				'hide' => true,
				'fields' => array(
					'color' => array(
						'type' => 'color',
						'label' => _x('Text Color', 'Widget Field Label', DOMAIN_NAMESPACE),
					),
				),
			),
		);
	}

	static function less_vars_def($instance) {
		return array(
			'color' => array('customize', 'color'),
		);
	}
}
