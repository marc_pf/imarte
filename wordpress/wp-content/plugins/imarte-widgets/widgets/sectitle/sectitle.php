<?php

namespace ImarteW;

class SecTitle extends Widget {

	function __construct() {
		parent::__construct( 'Imarte Section Title' );
	}

	// Creating widget front-end
	public function get_html_content( $instance, $args ) {
		$tag_num = $instance['customize']['tag'] ? $instance['customize']['tag'] : 2;
		return "<h{$tag_num} class='imarte-section-title'>{$instance['title']}</h{$tag_num}>";
	}

	public function get_widget_form() {

		return array(
			'title' => array(
				'type' => 'text',
				'label' => _x('Title', 'Widget Field Label', DOMAIN_NAMESPACE),
			),
			'customize' => array(
				'type' => 'section',
				'label' => _x('Customize', 'Widget Section Label', DOMAIN_NAMESPACE),
				'hide' => true,
				'fields' => array(
					'background' => array(
						'type' => 'color',
						'label' => _x('Background Color', 'Widget Field Label', DOMAIN_NAMESPACE),
					),
					'color' => array(
						'type' => 'color',
						'label' => _x('Text Color', 'Widget Field Label', DOMAIN_NAMESPACE),
					),
					'tag' => array(
						'type' => 'select',
						'label' => _x('Title Tag', 'Widget Field Label', DOMAIN_NAMESPACE),
						'default' => 2,
						'options' => array(
							1 => 'h1',
							2 => 'h2',
							3 => 'h3',
							4 => 'h4',
							5 => 'h5',
							6 => 'h6',
						),
					),
					'size' => array(
						'type' => 'measurement',
						'label' => _x('Font Size', 'Widget Field Label', DOMAIN_NAMESPACE),
					),
				),
			),
		);
	}

	static function less_vars_def($instance) {
		return array(
			'background' => array('customize', 'background'),
			'color' => array('customize', 'color'),
			'size' => array('customize', 'size'),
		);
	}
}
