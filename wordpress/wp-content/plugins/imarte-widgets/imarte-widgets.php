<?php
/*
Plugin Name: Imarte Widgets
*/

namespace ImarteW;
use \SiteOrigin_Widget;

const DOMAIN_NAMESPACE = 'imartew';

define(__NAMESPACE__ . '\\PLUGIN_PATH', plugin_dir_path(__FILE__));
define(__NAMESPACE__ . '\\PLUGIN_URL', plugins_url('/', __FILE__));

function load_imarte_widget($class) {
	$id = id_from_class($class);
	$full_id = get_full_id($id);
	$file = widget_class_file($class);
	include_once $file;
	siteorigin_widget_register($full_id, $file, get_full_class($class));
}

function id_from_class($class) {
	return strtolower($class);
}

function get_full_id($id) {
	return DOMAIN_NAMESPACE . "_{$id}";
}

function get_full_class($class) {
	return __NAMESPACE__ . "\\$class";
}

function widget_class_file($class) {
	$id = id_from_class($class);
	return PLUGIN_PATH . "widgets/{$id}/{$id}.php";
}

function widget_dir($class) {
	$id = id_from_class($class);
	return PLUGIN_PATH . "widgets/{$id}/";
}

function widget_url($class) {
	$id = id_from_class($class);
	return PLUGIN_URL . "widgets/{$id}/";
}

add_action( 'plugins_loaded', __NAMESPACE__ . '\\do_classes' );
function do_classes() {
	$widgets = array(
		'Separator',
		'SecTitle',
		'Single_Page_Menu',
		'Related_Pages_Menu',
		'Page_Title',
		'Children_Content',
		'Image',
	);

	class Widget extends SiteOrigin_Widget {
		function __construct($name, $description = null) {
			$short_class = substr(strrchr(get_class($this), '\\'), 1);
			$id = id_from_class($short_class);
			$full_id = get_full_id($id);
			$name = _x($name, 'Widget Name', DOMAIN_NAMESPACE);
			$args = array('has_preview' => false, 'panels_groups' => array('imarte') );

			if ($description) {
				$args['description'] = _x( $description, 'Widget Description', DOMAIN_NAMESPACE );
			}

			$this->short_class = $short_class;
			$this->id = $full_id;

			parent::__construct( $full_id, $name, $args);
		}

		function initialize() {
			$this->init_styles();

			if ( method_exists($this, 'sanitize_instance') ) {
				add_filter( 'siteorigin_widgets_sanitize_instance_' . $this->id_base, array($this, 'sanitize_instance' ), 10, 2);
			}
		}

		function init_styles() {
			$css_file = widget_dir($this->short_class) . 'css/style.css';
			$css_url = widget_url($this->short_class) . 'css/style.css';

			if ( file_exists($css_file) ) {
				$this->register_frontend_styles(
					array(
						array(
							$this->id,
							$css_url,
							array()
						),
					)
				);
			}
		}

		function get_less_variables($instance) {

			if (empty($instance)) return array();

			$vars = array();
			$vars_def = $this->less_vars_def($instance);

			foreach ( $vars_def as $var_name => $var_path ) {
				$value = static::get_instance_var($instance, $var_path);
				isset($value) && $vars[$var_name] = $value;
			}

			return $vars;
		}

		static function get_instance_var($instance, $path) {
			$value = $instance;
			foreach ((array)$path as $key) {
				if ( isset($value[$key]) ) $value = $value[$key];
				else return null;
			}
			return $value;
		}

		static function less_vars_def($instance) {
			return array();
		}
	}

	foreach ($widgets as $widget_class) {
		load_imarte_widget($widget_class);
	}
}

function add_widget_tabs($tabs) {
    $tabs[] = array(
        'title' => 'Imarte',
        'filter' => array(
            'groups' => array('imarte')
        )
    );

    return $tabs;
}
add_filter('siteorigin_panels_widget_dialog_tabs', __NAMESPACE__ . '\\add_widget_tabs', 20);

function add_widgets_to_tab($widgets){
	static $to_add = array(
		'SiteOrigin_Widget_Editor_Widget',
		'SiteOrigin_Widget_Slider_Widget',
	);

	foreach ($to_add as $widget) {
		$widgets[$widget]['groups'][] = 'imarte';
	}

    return $widgets;
}
add_filter('siteorigin_panels_widgets', __NAMESPACE__ . '\\add_widgets_to_tab', 20);
