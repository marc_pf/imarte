<?php
use Roots\Sage\Assets;
?>

<header class="banner">
    <div class="container banner__container">
        <div class='row banner__row'>
            <div class='banner__main banner__item'>
                <a class="brand banner__brand" href="<?= esc_url(home_url('/')); ?>">
                    <img class='brand__logo logo' src="<?= Assets\asset_path('images/logo.png'); ?>" />
                </a>
            </div>
            <nav class="menu banner__menu menu--lang banner__item">
                <?php //TODO: put lang selector here ?>
                <?php
                if (has_nav_menu('lang_navigation')) :
                    wp_nav_menu(array('theme_location' => 'lang_navigation', 'menu_class' => 'nav menu__nav'));
                endif;
                ?>
            </nav>
            <nav class="menu banner__menu menu--top banner__item">
                <?php
                if (has_nav_menu('top_navigation')) :
                    wp_nav_menu(array('theme_location' => 'top_navigation', 'menu_class' => 'nav menu__nav'));
                endif;
                ?>
            </nav>
            <div class='menu-button banner__menu-button banner__item'>
                <button class="hamburger hamburger--collapse js-show-menu" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</header>
