<?php use Roots\Sage\Titles; ?>

<div class="page-header">
    <?php if ( function_exists('yoast_breadcrumb') ) :
        yoast_breadcrumb('<div class="breadcrumbs page-header__breadcrumbs">','</div>');
    endif; ?>
    <?php if ( !function_exists('siteorigin_panels_is_panel') || !siteorigin_panels_is_panel() ) : ?>
        <h1 class='page-title'><?= Titles\title(); ?></h1>
    <?php endif; ?>
</div>
