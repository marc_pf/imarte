<div class="scroll-top">
  <a href="#" class="scroll-top__link js-scroll-top">&#8593;</a>
</div>
<footer class="content-info">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
