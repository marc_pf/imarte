<article <?php post_class('article col-md-6 col-lg-4'); ?>>
    <div class='article__inner'>
        <figure class='article__image'>
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail() ?>
            </a>
        </figure>
        <header class='article__header'>
            <h2 class="article__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        </header>
        <div class="entry-summary article__excerpt">
            <?php the_excerpt(); ?>
        </div>
        <div class='article__more'>
            <a href="<?php the_permalink(); ?>">+info</a>
        </div>
    </div>
</article>
